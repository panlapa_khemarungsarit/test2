from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.common.action_chains import ActionChains
import unittest


class NewVisitorTest(unittest.TestCase):

    def setUp(self):
        self.browser = webdriver.Firefox()
        self.browser.implicitly_wait(3)

    def tearDown(self):
        self.browser.quit()

    def test_check1_send_data(self):
        self.browser.get('http://localhost:8000')
        inputbox_date = self.browser.find_element_by_id('date_new')
        inputbox_cost = self.browser.find_element_by_id('cost_new')
        inputbox_date.send_keys('17/3/2558')
        inputbox_cost.send_keys('100')
        button = self.browser.find_element_by_id('button')
        button.click()
        table = self.browser.find_element_by_id('id_list_table')
        rows = table.find_elements_by_tag_name('tr')
        self.assertIn('17/3/2558 100.0', [row.text for row in rows])

    def test_check2_sum(self):
        self.browser.get('http://localhost:8000')
        inputbox_date = self.browser.find_element_by_id('date_new')
        inputbox_cost = self.browser.find_element_by_id('cost_new')
        inputbox_date.send_keys('18/3/2558')
        inputbox_cost.send_keys('200')
        button = self.browser.find_element_by_id('button')
        button.click()
        table = self.browser.find_element_by_id('id_list_table')
        rows = table.find_elements_by_tag_name('tr')
        self.assertIn('ผลรวม 300.0', [row.text for row in rows])

    def test_check3_avg(self):
        self.browser.get('http://localhost:8000')
        inputbox_date = self.browser.find_element_by_id('date_new')
        inputbox_cost = self.browser.find_element_by_id('cost_new')
        inputbox_date.send_keys('19/3/2558')
        inputbox_cost.send_keys('300.0')
        button = self.browser.find_element_by_id('button')
        button.click()
        table = self.browser.find_element_by_id('id_list_table')
        rows = table.find_elements_by_tag_name('tr')
        self.assertIn('ค่าเฉลี่ย 200.0', [row.text for row in rows])

    def test_check4_min(self):
        self.browser.get('http://localhost:8000')
        table = self.browser.find_element_by_id('id_list_table')
        rows = table.find_elements_by_tag_name('tr')
        self.assertIn('ค่าMIN 100.0', [row.text for row in rows])

    def test_check5_max(self):
        self.browser.get('http://localhost:8000')
        table = self.browser.find_element_by_id('id_list_table')
        rows = table.find_elements_by_tag_name('tr')
        self.assertIn('ค่าMAX 300.0', [row.text for row in rows])

    def test_check6_min_to_max(self):
        self.browser.get('http://localhost:8000')
        b_min = self.browser.find_element_by_id('button_min')
        b_min.click()
        table = self.browser.find_element_by_id('id_list_table')
        rows = table.find_elements_by_tag_name('tr')
        self.assertTrue(
           ['date money delete',
            '17/3/2558 100.0',
            '18/3/2558 200.0',
            '19/3/2558 300.0',
            'ผลรวม 600.0',
            'ค่าเฉลี่ย 200.0',
            'ค่าMAX 300.0',
            'ค่าMIN 100.0'], [row.text for row in rows])

    def test_check7_max_to_min(self):
        self.browser.get('http://localhost:8000')
        b_min = self.browser.find_element_by_id('button_max')
        b_min.click()
        table = self.browser.find_element_by_id('id_list_table')
        rows = table.find_elements_by_tag_name('tr')
        self.assertTrue(
           ['date money delete',
            '19/3/2558 300.0',
            '18/3/2558 200.0',
            '17/3/2558 100.0',
            'ผลรวม 600.0',
            'ค่าเฉลี่ย 200.0',
            'ค่าMAX 300.0',
            'ค่าMIN 100.0'], [row.text for row in rows])

if __name__ == '__main__':
    unittest.main(warnings='ignore')
