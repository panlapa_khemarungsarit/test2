from django.conf.urls import patterns, include, url
from django.contrib import admin

urlpatterns = patterns('',
    url(r'^$', 'list_money.views.home', name='home'),
    url(r'^mintomax/$', 'list_money.views.mintomax', name='mintomax'),
    url(r'^maxtomin/$', 'list_money.views.maxtomin', name='maxtomin'),
)
