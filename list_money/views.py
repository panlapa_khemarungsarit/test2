from django.shortcuts import redirect, render
from list_money.models import money
import operator
# Create your views here.


def make_list(moneys):
    list_ = []
    list_row = []
    for list_m in moneys:
        list_.append(list_m.date)
        list_.append(list_m.cost)
        list_.append(list_m.id)
        list_row.append(list_)
        list_ = []
    return list_row


def delete(request):
    id_data = request.POST['id_delete']
    money.objects.get(pk=id_data).delete()


def create(request):
    date_text = request.POST['date']
    cost_text = float(request.POST['cost'])
    cost_text = round(cost_text, 2)
    money.objects.create(
            date=date_text,
            cost=cost_text
               )


def min_data(list_cost):
    if(len(list_cost) == 0):
        return 0
    return min(list_cost)[1]


def max_data(list_cost):
    if(len(list_cost) == 0):
        return 0
    return max(list_cost)[1]


def avg_data(n, cost):
    if(n != 0):
        return round(cost/n, 2)
    else:
        return 0


def home(request):
    if(request.method == 'POST' and
            request.POST.get('delete', '') == 'delete'):
        delete(request)
        return redirect('/')
    if(request.method == 'POST'and
            request.POST.get('button_send', '') == 'send'):
        create(request)
        return redirect('/')
    if(request.method == 'POST'and
            request.POST.get('button_min', '') == 'min'):
        return redirect('/mintomax')
    if(request.method == 'POST'and
            request.POST.get('button_max', '') == 'max'):
        return redirect('/maxtomin')
    moneys = money.objects.all()
    list_money = make_list(moneys)
    cost = 0
    n = 0
    for m in moneys:
        cost = cost+m.cost
        n = n+1
    avg = avg_data(n, cost)
    min_m = min_data(list_money)
    max_m = max_data(list_money)
    return render(request, 'home.html', {
                'moneys': list_money,
                'cost_money': cost,
                'avg': avg,
                'min': min_m,
                'max': max_m,
                'minormax': 3})


def mintomax(request):
    if(request.method == 'POST' and
            request.POST.get('delete', '') == 'delete'):
        delete(request)
        return redirect('/mintomax')
    if(request.method == 'POST'and
            request.POST.get('button_send', '') == 'send'):
        create(request)
        return redirect('/mintomax')
    if(request.method == 'POST'and
            request.POST.get('button_max', '') == 'max'):
        return redirect('/maxtomin')
    moneys = money.objects.all()
    list_money = make_list(moneys)
    list_money = sorted(list_money, key=operator.itemgetter(1))
    cost = 0
    n = 0
    for m in moneys:
        cost = cost+m.cost
        n = n+1
    avg = avg_data(n, cost)
    min_m = min_data(list_money)
    max_m = max_data(list_money)
    return render(request, 'home.html', {
                'moneys': list_money,
                'cost_money': cost,
                'avg': avg,
                'min': min_m,
                'max': max_m,
                'minormax': 2})


def maxtomin(request):
    if(request.method == 'POST' and
            request.POST.get('delete', '') == 'delete'):
        delete(request)
        return redirect('/maxtomin')
    if(request.method == 'POST'and
            request.POST.get('button_send', '') == 'send'):
        create(request)
        return redirect('/maxtomin')
    if(request.method == 'POST' and
            request.POST.get('button_min', '') == 'min'):
        return redirect('/mintomax')
    moneys = money.objects.all()
    list_money = make_list(moneys)
    list_money = sorted(list_money, key=operator.itemgetter(1), reverse=True)
    cost = 0
    n = 0
    for m in moneys:
        cost = cost+m.cost
        n = n+1
    avg = avg_data(n, cost)
    min_m = min_data(list_money)
    max_m = max_data(list_money)
    return render(request, 'home.html', {
                'moneys': list_money,
                'cost_money': cost,
                'avg': avg,
                'min': min_m,
                'max': max_m,
                'minormax': 1})
