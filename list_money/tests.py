from django.core.urlresolvers import resolve
from django.http import HttpRequest
from django.template.loader import render_to_string
from django.test import TestCase
import pep8
from list_money.models import money
from list_money.views import home


class HomeTest(TestCase):

    def test_pep8_conformance(self):
        pep8style = pep8.StyleGuide(quiet=True)
        result = pep8style.check_files([
            'list_money/views.py',
            'list_money/models.py',
            'functional_tests.py'])
        self.assertEqual(
            result.total_errors,
            0,
            "Found code style errors (and warnings).")

    def test_show(self):
        money.objects.create(date='10/2/2558', cost=200)
        request = HttpRequest()
        response = home(request)
        self.assertIn('10/2/2558', response.content.decode())
        self.assertIn('200', response.content.decode())

    def test_send_to_db(self):
        request = HttpRequest()
        request.method = 'POST'
        request.POST['date'] = '11/2/2558'
        request.POST['cost'] = 200
        request.POST['button_send'] = 'send'
        response = home(request)
        m = money.objects.first()
        self.assertEqual(m.date, '11/2/2558')
        self.assertEqual(m.cost, 200)

    def test_sum_show(self):
        money.objects.create(date='12/2/2558', cost=200)
        money.objects.create(date='13/2/2558', cost=200)
        request = HttpRequest()
        response = home(request)
        self.assertIn('400', response.content.decode())

    def test_avg_show(self):
        money.objects.create(date='12/2/2558', cost=200)
        money.objects.create(date='13/2/2558', cost=100)
        request = HttpRequest()
        response = home(request)
        self.assertIn('150', response.content.decode())
